#version 450

const float PHI = 1.61803398874989484820459; // Golden Ratio
const float SEED = 347457016375973.;

layout(location = 0) in vec2 tex_coords;
layout(location = 0) out vec4 f_color;

//layout(set = 0, binding = 0) uniform Uniforms {
//    float u_time;
//    vec2 u_resolution;
//    vec2 u_mouse;
//};

layout(set = 0, binding = 0) uniform Uniforms {
    float t;
    float rx;
    float ry;
    float mx;
    float my;
};

float hue2rgb(float f1, float f2, float hue) {
    if (hue < 0.0)
        hue += 1.0;
    else if (hue > 1.0)
        hue -= 1.0;
    float res;
    if ((6.0 * hue) < 1.0)
        res = f1 + (f2 - f1) * 6.0 * hue;
    else if ((2.0 * hue) < 1.0)
        res = f2;
    else if ((3.0 * hue) < 2.0)
        res = f1 + (f2 - f1) * ((2.0 / 3.0) - hue) * 6.0;
    else
        res = f1;
    return res;
}
vec3 hsl2rgb(vec3 hsl) {
    vec3 rgb;
    if (hsl.y == 0.0) {
        rgb = vec3(hsl.z); // Luminance
    } else {
        float f2;
        if (hsl.z < 0.5)
            f2 = hsl.z * (1.0 + hsl.y);
        else
            f2 = hsl.z + hsl.y - hsl.y * hsl.z;
        float f1 = 2.0 * hsl.z - f2;
        rgb.r = hue2rgb(f1, f2, hsl.x + (1.0/3.0));
        rgb.g = hue2rgb(f1, f2, hsl.x);
        rgb.b = hue2rgb(f1, f2, hsl.x - (1.0/3.0));
    }
    return rgb;
}
vec3 hsl2rgb(float h, float s, float l) {
    return hsl2rgb(vec3(h, s, l));
}

float random(vec2 st) {
    return fract(sin(dot(st.xy, vec2(12.9898,78.233))) * 43758.5453123);
}

float random(float x, float y) {
    return random(vec2(x, y));
}

float line(float pos, float thickness, float y) {
    float height = 0.5;

    float py = pos - height * y;
    float s = smoothstep(py - thickness / 2., py + thickness - thickness / 2., tex_coords.y);

    return 1. - (step(s, 0.) + step(1., s));
}

void main() {
    float pi = 3.141592653589793;
    f_color = vec4(0., 0., 0., 0.);

    for (int i = 1; i <= 50; i++) {
        /* float y = sin(tex_coords.x * pi * (i * random(vec2(i, 1.))) + t); */
        /* y *= sin(tex_coords.x * pi * random(vec2(i, 0.)) + t); */

        float y = sin(tex_coords.x * pi + t + (i * 2.)) * sign(sin(i) * 2. - 1.) * sin(t + i * 2.);

        vec3 color = vec3(0.);

        /* color += sin(tex_coords.x + t) * vec3(sin(i / 3. + t), 0., sin(random(vec2(i, 0.)) * 3. + t)); */
        /* color += sin(tex_coords.x + t) * vec3(sin(i / 3. + t), 0., sin(random(vec2(i, 0.)) * 3. + t)); */

        /* color = hsl2rgb(sin(tex_coords.x + t * random(i, 0.)), 0.9, 0.2) * sin(tex_coords.x + t * i); */
        color += hsl2rgb(sin(tex_coords.x * pi - t), 0.9, 0.2);

        float line = line(0.5, 0.005, y);

        f_color += vec4(line * color, 1.);
        /* f_color += vec4(line * vec3(1.), 1.); */
    }


    /* if (tex_coords.y > 0.499 && tex_coords.y < 0.501) { */
        /* f_color = vec4(1., 0., 0., 1.); */
    /* } */
}


/* float gold_noise(vec2 xy, float seed) { */
/*     return fract(tan(distance(xy*PHI, xy)*seed)*xy.x); */
/* } */

/* float map(float value, float min1, float max1, float min2, float max2) { */
/*   return min2 + (value - min1) * (max2 - min2) / (max1 - min1); */
/* } */
