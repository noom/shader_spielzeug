#version 450

const float PHI = 1.61803398874989484820459; // Golden Ratio
const float SEED = 347457016375973.;

layout(location = 0) in vec2 tex_coords;
layout(location = 0) out vec4 f_color;

//layout(set = 0, binding = 0) uniform Uniforms {
//    float u_time;
//    vec2 u_resolution;
//    vec2 u_mouse;
//};

layout(set = 0, binding = 0) uniform Uniforms {
    float t;
    float rx;
    float ry;
    float mx;
    float my;
};


void main() {
    vec3 c;
    float l;
    float z = t;

    for (int i = 0; i < 3; i++) {
        vec2 p = tex_coords.xy;
        vec2 uv = p;

        p -= 0.5;
        p.x *= rx / ry;

        z += 0.07;

        l = length(p);

        uv += p / l * (sin(z) + 1.) * abs(sin(l * 9. - z * 2.));
        /* uv += p / length(p) * (sin(z)); */

        /* uv += p / l * (sin(z) + 1.); */
        /* uv *= 30. + sin(t / 2.) * 10.; */

        c[i] = 0.005 / length(abs(mod(uv, 1.) - 0.5)) / l;
        /* c[i] = 0.005 / length(abs(mod(uv, 1.) - 0.5)); */
    }

    f_color = vec4(c, t);
}

/* float random (vec2 st) { */
/*     return fract(sin(dot(st.xy, vec2(12.9898,78.233))) * 43758.5453123); */
/* } */

/* float gold_noise(vec2 xy, float seed) { */
/*     return fract(tan(distance(xy*PHI, xy)*seed)*xy.x); */
/* } */

/* float hue2rgb(float f1, float f2, float hue) { */
/*     if (hue < 0.0) */
/*         hue += 1.0; */
/*     else if (hue > 1.0) */
/*         hue -= 1.0; */
/*     float res; */
/*     if ((6.0 * hue) < 1.0) */
/*         res = f1 + (f2 - f1) * 6.0 * hue; */
/*     else if ((2.0 * hue) < 1.0) */
/*         res = f2; */
/*     else if ((3.0 * hue) < 2.0) */
/*         res = f1 + (f2 - f1) * ((2.0 / 3.0) - hue) * 6.0; */
/*     else */
/*         res = f1; */
/*     return res; */
/* } */
/* vec3 hsl2rgb(vec3 hsl) { */
/*     vec3 rgb; */
/*     if (hsl.y == 0.0) { */
/*         rgb = vec3(hsl.z); // Luminance */
/*     } else { */
/*         float f2; */
/*         if (hsl.z < 0.5) */
/*             f2 = hsl.z * (1.0 + hsl.y); */
/*         else */
/*             f2 = hsl.z + hsl.y - hsl.y * hsl.z; */
/*         float f1 = 2.0 * hsl.z - f2; */
/*         rgb.r = hue2rgb(f1, f2, hsl.x + (1.0/3.0)); */
/*         rgb.g = hue2rgb(f1, f2, hsl.x); */
/*         rgb.b = hue2rgb(f1, f2, hsl.x - (1.0/3.0)); */
/*     } */
/*     return rgb; */
/* } */
/* vec3 hsl2rgb(float h, float s, float l) { */
/*     return hsl2rgb(vec3(h, s, l)); */
/* } */

/* float map(float value, float min1, float max1, float min2, float max2) { */
/*   return min2 + (value - min1) * (max2 - min2) / (max1 - min1); */
/* } */
