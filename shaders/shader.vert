#version 450

layout(location = 0) in vec2 position;
layout(location = 0) out vec2 tex_coords;

void main() {
    gl_Position = vec4(position, 0., 1.);
    tex_coords = (position + vec2(1.)) * 0.5;

    /* tex_coords = position.xy * vec2(0.5) + vec2(0.5); */
    /* gl_Position = vec4(position.xy, 0., 1.); */
}

/* const vec2 madd=vec2(0.5,0.5); */
/* attribute vec2 position; */
/* varying vec2 textureCoord; */
/* void main() { */
/*    textureCoord = position.xy*madd+madd; // scale vertex attribute to [0-1] range */
/*    gl_Position = vec4(position.xy,0.0,1.0); */
/* } */
