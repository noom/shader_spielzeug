/* https://www.youtube.com/watch?v=cQXAbndD5CQ */

#version 450
precision highp float;

const float PI = 3.14159265359;
const float TWO_PI = 6.28318530718;

layout(location = 0) in vec2 frag_coord;
layout(location = 0) out vec4 frag_color;

layout(set = 0, binding = 0) uniform Uniforms {
    float time; // time
    vec2 resolution;
    vec2 mouse; // normalized mouse position
};

float to_lin(in float s) {
    float linear;
    if (s <= 0.04045) linear = s / 12.92;
    else linear = pow((s + 0.055) / 1.055, 2.4);
    return linear;
}
vec3 to_lin(in vec3 color) {
    return vec3(to_lin(color.r), to_lin(color.g), to_lin(color.b));
}

float xor(float a, float b) {
    return a + b - 2 * a * b;
}

mat2 rotate(float angle) {
    return mat2(cos(angle), -sin(angle), sin(angle), cos(angle));
}

void main() {
    vec2 uv = (gl_FragCoord.xy - resolution.xy / 2.) / resolution.y;
    uv *= rotate(PI / 4.); // rotate
    uv *= 13.; // scale
    uv += 0.5; // center

    vec2 gv = fract(uv) - 0.5;
    vec2 id = floor(uv);

    vec3 c = vec3(0.);

    float circles1 = 0.;
    float circles2 = 0.;

    float circle2_curve = pow(sin(-time * 0.6) * 0.5 + 0.5, 5.);
    float circle2_off = 0.04 + 0.03 * circle2_curve;

    for (float y = -1.; y <= 1.; y += 1.) {
        for (float x = -1.; x <= 1.; x += 1.) {
            vec2 offs = vec2(x, y);

            float dist = length(id + offs);

            float circle1 = length(gv - offs);
            float circle2 = length(gv - offs + circle2_off);

            float radius = mix(0.3, 1.5, sin(time * 0.8 + dist) * 0.5 + 0.5);

            /* float blur_curve = pow(abs(sin(PI * (-time * 0.5) / 2. + dist)), 3.5); */
            float blur_curve = 1. - pow(max(0., abs(sin(-time + dist) * 0.5 + 0.5) * 2. - 1.), 0.5);
            float blur = 0.03 + 0.07 * blur_curve;

            circles1 = xor(circles1, 1. - smoothstep(radius - blur, radius, circle1));
            circles2 = xor(circles2, 1. - smoothstep(radius - blur, radius, circle2));
        }
    }

    /* c += mod(circles, 2.); */
    c.r += circles1;
    c.b += circles2;

    /* c.rg += gv; */
    frag_color = vec4(to_lin(c), 1.);
}
