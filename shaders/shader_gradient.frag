#version 450
precision highp float;

const float PI = 3.14159265359;
const float TWO_PI = 6.28318530718;
const int COLORS_BUF_SIZE = 256;

layout(location = 0) in vec2 tex_coords;
layout(location = 0) out vec4 f_color;

layout(set = 0, binding = 0) uniform Uniforms {
    float t; // time
    float rx; // resolution
    float ry;
    float mx; // normalized mouse position
    float my;
};

float to_lin(in float s) {
    float linear;
    if (s <= 0.04045) linear = s / 12.92;
    else linear = pow((s + 0.055) / 1.055, 2.4);
    return linear;
}
vec3 to_lin(in vec3 color) {
    return vec3(to_lin(color.r), to_lin(color.g), to_lin(color.b));
}

vec3 hsb2rgb( in vec3 c ){
    vec3 rgb = clamp(abs(mod(c.x*6.0+vec3(0.0,4.0,2.0),
                             6.0)-3.0)-1.0,
                     0.0,
                     1.0 );
    rgb = rgb*rgb*(3.0-2.0*rgb);
    return c.z * mix( vec3(1.0), rgb, c.y);
}


float map(float value, float min1, float max1, float min2, float max2) {
  return min2 + (value - min1) * (max2 - min2) / (max1 - min1);
}

float random (vec2 st) {
    return fract(sin(dot(st.xy, vec2(12.9898,78.233))) * 43758.5453123);
}

vec3 ran_color(float x) {
    vec3 color = hsb2rgb(vec3(random(vec2(x) * 1.), 0.65, 0.5));
    return color;
}

vec3[COLORS_BUF_SIZE] ran_colors(float seed, int count) {
    vec3[COLORS_BUF_SIZE] colors;
    for (int i = 0; i < count; i++) {
        colors[i] = ran_color(seed * i);
    }
    return colors;
}

vec3 ran_gradiant(int count, float range, float off, float transition_length, float value) {
    vec3 color = vec3(0.);

    vec3 first_color;
    for (int i = 0; i < count; i++) {
        vec3 color_step = ran_color(i + 1);

        value = fract(value);

        if (i == 0) {
            first_color = color_step;
        }

        float first = step(1., i);
        float pct = smoothstep(
            (range / count * i - transition_length + off) * first,
            (range / count * i + transition_length + off) * first,
            value
        );

        color = mix(color, color_step, pct);

        if (i + 1 == count) {
            pct = smoothstep(
                range - transition_length,
                range + transition_length,
                value
            );
            color = mix(color, first_color, pct);
        }
    }

    return color;
}

vec3 gradiant(
    vec3[COLORS_BUF_SIZE] colors,
    int colors_len,
    float range,
    float off,
    float transition_length,
    float value
) {
    vec3 color = vec3(0.);

    for (int i = 0; i < colors_len; i++) {
        value = fract(value);

        float first = step(1., i);
        float pct = smoothstep(
            (range / colors_len * i - transition_length + off) * first,
            (range / colors_len * i + transition_length + off) * first,
            value
        );
        color = mix(color, colors[i], pct);

        if (i + 1 == colors_len) {
            pct = smoothstep(
                range - transition_length,
                range,
                value
            );
            color = mix(color, colors[0], pct);
        } else if (i == 0) {
            pct = smoothstep(
                0.,
                0. + transition_length,
                value
            );
            /* color = mix(color, colors[colors_len - 1], pct); */
        }
    }

    return color;
}

void main() {
    // Nice purple/yellow/blue
    /* vec4 bc = vec4(to_lin(hsl2rgb(map(b, 0., 0.36, 0., 1.), 0.7, 0.4)), b * 3.7); */
    /* vec4 bc = vec4(hsl2rgb(map(b, 0.5, 1., 0., 1.), 0.7, 0.4), b * 3.7); */
    /* vec4 bc = vec4(hsl2rgb(map(b, 0., 0.3, 0., 1.), 0.7, 0.4), b * 3.7); */

    /* vec3 color = ran_gradiant(10, 0.4, 0.10, 0.005, tex_coords.x + delta); */
    /* vec3 color = ran_gradiant(6, 1., 0., 0.05, tex_coords.x + t / 8); */

    vec3[COLORS_BUF_SIZE] colors = ran_colors(0.112313, 6);

    /* vec3 color = ran_gradiant(6, 1., 0., 0.05, tex_coords.x + t / 8); */
    vec3 color = gradiant(colors, 6, 1., 0., 0.05, tex_coords.x + t / 8);

    vec4 bc = vec4(to_lin(color), 1.);

    f_color = bc;
}

/* void main(){ */
/*     vec2 st = vec2(tex_coords.x, 1. - tex_coords.y); */
/*     vec3 color = vec3(0.); */

/*     // Use polar coordinates instead of cartesian */
/*     vec2 toCenter = vec2(0.5)-st; */
/*     float angle = atan(toCenter.y,toCenter.x); */
/*     float radius = length(toCenter)*2.0; */

/*     // Map the angle (-PI to PI) to the Hue (from 0 to 1) */
/*     // and the Saturation to the radius */
/*     color = hsb2rgb(vec3((angle/TWO_PI)+0.5,radius,1.0)); */

/*     f_color = vec4(to_lin(color), 1.); */
/* } */
