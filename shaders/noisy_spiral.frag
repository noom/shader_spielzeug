/* https://www.youtube.com/watch?v=cQXAbndD5CQ */

#version 450
precision highp float;

const float PI = 3.14159265359;
const float TWO_PI = 6.28318530718;

layout(location = 0) in vec2 frag_coord;
layout(location = 0) out vec4 frag_color;

layout(set = 0, binding = 0) uniform Uniforms {
    float time; // time
    vec2 resolution;
    vec2 mouse; // normalized mouse position
};

float to_lin(in float s) {
    float linear;
    if (s <= 0.04045) linear = s / 12.92;
    else linear = pow((s + 0.055) / 1.055, 2.4);
    return linear;
}
vec3 to_lin(in vec3 color) {
    return vec3(to_lin(color.r), to_lin(color.g), to_lin(color.b));
}

float xor(float a, float b) {
    return a + b - 2 * a * b;
}

mat2 rotate(float angle) {
    return mat2(cos(angle), -sin(angle), sin(angle), cos(angle));
}

vec3 mod289(vec3 x) {
    return x - floor(x * (1.0 / 289.0)) * 289.0;
}
vec4 mod289(vec4 x) {
    return x - floor(x * (1.0 / 289.0)) * 289.0;
}
vec4 permute(vec4 x) {
    return mod289(((x*34.0)+1.0)*x);
}
vec4 taylorInvSqrt(vec4 r)
{
    return 1.79284291400159 - 0.85373472095314 * r;
}
float snoise(vec3 v)
{
    const vec2  C = vec2(1.0/6.0, 1.0/3.0) ;
    const vec4  D = vec4(0.0, 0.5, 1.0, 2.0);

    // First corner
    vec3 i  = floor(v + dot(v, C.yyy) );
    vec3 x0 =   v - i + dot(i, C.xxx) ;

    // Other corners
    vec3 g = step(x0.yzx, x0.xyz);
    vec3 l = 1.0 - g;
    vec3 i1 = min( g.xyz, l.zxy );
    vec3 i2 = max( g.xyz, l.zxy );

    //   x0 = x0 - 0.0 + 0.0 * C.xxx;
    //   x1 = x0 - i1  + 1.0 * C.xxx;
    //   x2 = x0 - i2  + 2.0 * C.xxx;
    //   x3 = x0 - 1.0 + 3.0 * C.xxx;
    vec3 x1 = x0 - i1 + C.xxx;
    vec3 x2 = x0 - i2 + C.yyy; // 2.0*C.x = 1/3 = C.y
    vec3 x3 = x0 - D.yyy;      // -1.0+3.0*C.x = -0.5 = -D.y

    // Permutations
    i = mod289(i); 
    vec4 p = permute( permute( permute( 
                    i.z + vec4(0.0, i1.z, i2.z, 1.0 ))
                + i.y + vec4(0.0, i1.y, i2.y, 1.0 )) 
            + i.x + vec4(0.0, i1.x, i2.x, 1.0 ));

    // Gradients: 7x7 points over a square, mapped onto an octahedron.
    // The ring size 17*17 = 289 is close to a multiple of 49 (49*6 = 294)
    float n_ = 0.142857142857; // 1.0/7.0
    vec3  ns = n_ * D.wyz - D.xzx;

    vec4 j = p - 49.0 * floor(p * ns.z * ns.z);  //  mod(p,7*7)

    vec4 x_ = floor(j * ns.z);
    vec4 y_ = floor(j - 7.0 * x_ );    // mod(j,N)

    vec4 x = x_ *ns.x + ns.yyyy;
    vec4 y = y_ *ns.x + ns.yyyy;
    vec4 h = 1.0 - abs(x) - abs(y);

    vec4 b0 = vec4( x.xy, y.xy );
    vec4 b1 = vec4( x.zw, y.zw );

    //vec4 s0 = vec4(lessThan(b0,0.0))*2.0 - 1.0;
    //vec4 s1 = vec4(lessThan(b1,0.0))*2.0 - 1.0;
    vec4 s0 = floor(b0)*2.0 + 1.0;
    vec4 s1 = floor(b1)*2.0 + 1.0;
    vec4 sh = -step(h, vec4(0.0));

    vec4 a0 = b0.xzyw + s0.xzyw*sh.xxyy ;
    vec4 a1 = b1.xzyw + s1.xzyw*sh.zzww ;

    vec3 p0 = vec3(a0.xy,h.x);
    vec3 p1 = vec3(a0.zw,h.y);
    vec3 p2 = vec3(a1.xy,h.z);
    vec3 p3 = vec3(a1.zw,h.w);

    //Normalise gradients
    vec4 norm = taylorInvSqrt(vec4(dot(p0,p0), dot(p1,p1), dot(p2, p2), dot(p3,p3)));
    p0 *= norm.x;
    p1 *= norm.y;
    p2 *= norm.z;
    p3 *= norm.w;

    // Mix final noise value
    vec4 m = max(0.6 - vec4(dot(x0,x0), dot(x1,x1), dot(x2,x2), dot(x3,x3)), 0.0);
    m = m * m;
    return 42.0 * dot( m*m, vec4( dot(p0,x0), dot(p1,x1), 
                dot(p2,x2), dot(p3,x3) ) );
}

float random(vec2 st) {
    return fract(sin(dot(st.xy, vec2(12.9898,78.233))) * 43758.5453123);
}
float random(float x, float y) {
    return random(vec2(x, y));
}

float hue2rgb(float f1, float f2, float hue) {
    if (hue < 0.0)
        hue += 1.0;
    else if (hue > 1.0)
        hue -= 1.0;
    float res;
    if ((6.0 * hue) < 1.0)
        res = f1 + (f2 - f1) * 6.0 * hue;
    else if ((2.0 * hue) < 1.0)
        res = f2;
    else if ((3.0 * hue) < 2.0)
        res = f1 + (f2 - f1) * ((2.0 / 3.0) - hue) * 6.0;
    else
        res = f1;
    return res;
}
vec3 hsl2rgb(vec3 hsl) {
    vec3 rgb;
    if (hsl.y == 0.0) {
        rgb = vec3(hsl.z); // Luminance
    } else {
        float f2;
        if (hsl.z < 0.5)
            f2 = hsl.z * (1.0 + hsl.y);
        else
            f2 = hsl.z + hsl.y - hsl.y * hsl.z;
        float f1 = 2.0 * hsl.z - f2;
        rgb.r = hue2rgb(f1, f2, hsl.x + (1.0/3.0));
        rgb.g = hue2rgb(f1, f2, hsl.x);
        rgb.b = hue2rgb(f1, f2, hsl.x - (1.0/3.0));
    }
    return rgb;
}
vec3 hsl2rgb(float h, float s, float l) {
    return hsl2rgb(vec3(h, s, l));
}

void main() {
    vec2 og = (gl_FragCoord.xy - resolution.xy / 2.) / resolution.y;

    // polar coordinate system
    vec2 st = vec2(atan(og.x, og.y), length(og));
    vec2 uv = vec2(st.x / TWO_PI + 0.5, st.y);

    /* uv *= 25.; // scale */
    /* uv += 0.5; // center */
    /* vec2 gv = fract(uv) - 0.5; */
    /* vec2 id = floor(uv); */

    vec3 c = vec3(0.);

    // bubbly chaotic movements
    float noise = snoise(vec3(abs(uv.x - 0.5) * 20., abs(uv.y - 0.5) * 20., time * 0.4)) * 0.5 + 0.5;

    // trying to achieve wobbly circle
    float detail_amount = 10.;
    /* float noise = snoise(vec3(vec2(cos(st.x), sin(st.x)) * detail_amount, time * 0.25)) * 0.5 + 0.5; */

    /* c += vec3(1.) * (smoothstep(uv.y, uv.y + 0.01, noise / 2.)); */
    float amp = 0.55;
    // filled
    c += vec3(1.) * smoothstep(uv.y, uv.y + 0.05, 0.05 + noise * amp);

    // outlined
    /* float thickness = 0.008; */
    /* c += smoothstep(uv.y + thickness * 2., uv.y, 0.1 + noise * amp) */
    /*     * smoothstep(uv.y - thickness * 2., 0.1 + noise * amp, uv.y); */
    /* c += vec3(1.) */
    /*     * step(uv.y - thickness / 2., 0.1 + noise * amp) */
    /*     * step(0.1 + noise * amp, uv.y + thickness / 2.); */

    // colorize
    float color_noise = snoise(vec3(abs(uv.x - 0.5) * 20., abs(uv.y - 0.5) * 20., time * 0.4)) * 0.5 + 0.5;
    float noise_ease = max(0., smoothstep(0., 1., st.y) + 0.05);
    c *= hsl2rgb(vec3(mod(uv.x + uv.y * 10. + time / 2. + color_noise * noise_ease, 1.), 0.6, 0.5));
    /* c = vec3(noise_ease); */

    /* float amp = 0.5; */
    /* c += vec3(1.) * smoothstep(0., 0.005, uv.y + (sin(noise) * 0.5 + 0.5) * amp - amp / 2.); */
    /* c += vec3(1.) * smoothstep(0., 0.1, noise + uv.y - 0.5); */
    /* c += vec3(1. - smoothstep(0., PI, st.x + noise)); */

    frag_color = vec4(to_lin(c), 1.);
}
