use crate::shader::Vertex;

pub const VERTICES: [Vertex; 4] = [
    Vertex {
        position: [-1.0, -1.0],
    },
    Vertex {
        position: [-1.0, 1.0],
    },
    Vertex {
        position: [1.0, -1.0],
    },
    Vertex {
        position: [1.0, 1.0],
    },
];

pub const DEFAULT_VS: &str = r#"
#version 450

layout(location = 0) in vec2 position;
layout(location = 0) out vec2 tex_coords;

void main() {
    gl_Position = vec4(position, 0.0, 1.0);
    tex_coords = (position + vec2(1.0)) * 0.5;
}
"#;

pub const DEFAULT_FS: &str = r#"
#version 450

layout(location = 0) in vec2 tex_coords;
layout(location = 0) out vec4 f_color;

layout(set = 0, binding = 0) uniform Uniforms {
    float t;
    float rx;
    float ry;
    float mx;
    float my;
};

void main() {
}
"#;
