use nannou::{prelude::*, wgpu};

use notify::{
    event::{CreateKind, DataChange, EventKind, ModifyKind},
    RecommendedWatcher, RecursiveMode, Watcher,
};

use glsl_layout::AsStd140;

use std::sync::mpsc;

mod consts;
mod shader;
mod sim;

use consts::*;
use shader::{
    compile_custom_shader, compile_default_shader, create_frag_shader_pipeline, FragmentUniforms,
    UFragmentUniforms,
};
use sim::*;

#[derive(Debug, PartialEq)]
pub enum ShaderIOEvent {
    Update,
    Nothing,
}

fn main() {
    nannou::app(model).update(update).view(view).run();
}

fn model(app: &App) -> Model {
    let (sender, receiver) = mpsc::channel::<notify::Event>();

    let mut watcher: RecommendedWatcher = Watcher::new_immediate(move |res| match res {
        Ok(event) => {
            sender.send(event).unwrap();
        }
        Err(e) => panic!(e),
    })
    .unwrap();

    watcher
        .watch("./shaders", RecursiveMode::Recursive)
        .unwrap();

    app.set_loop_mode(LoopMode::rate_fps(60.));
    app.set_exit_on_escape(false);
    let w_id = app.new_window().build().unwrap();
    let window = app.window(w_id).unwrap();
    window.set_fullscreen(false);

    let shader_artifacts = match compile_custom_shader() {
        Ok((vert_artifact, frag_artifact)) => (vert_artifact, frag_artifact),
        Err(e) => {
            println!("{}", e);
            compile_default_shader()
        }
    };

    let fs_pipeline = create_frag_shader_pipeline(app, shader_artifacts);

    Model::new(watcher, receiver, fs_pipeline, 1.)
}

fn update(app: &App, model: &mut Model, _update: Update) {
    if let Ok(event) = model.shader_io_receiver.try_recv() {
        match event.kind {
            EventKind::Modify(ModifyKind::Data(DataChange::Content))
            | EventKind::Create(CreateKind::File) => {
                let mut should_update = false;
                for path in &event.paths {
                    if let Some(file_name) = path.file_name() {
                        let file_name = file_name.to_str().unwrap_or("");
                        if file_name == "shader.frag" || file_name == "shader.vert" {
                            should_update = true;
                        }
                    }
                }

                if !should_update {
                    return;
                }
            }
            _ => return,
        }

        println!("recompiling...");

        let shader_artifacts = match compile_custom_shader() {
            Ok((vert_artifact, frag_artifact)) => {
                println!("done.");
                (vert_artifact, frag_artifact)
            }
            Err(e) => {
                println!("{}", e);
                return;
            }
        };

        model.fs_pipeline = create_frag_shader_pipeline(app, shader_artifacts);

        std::thread::sleep(std::time::Duration::from_millis(50));
    }
}

fn view(app: &App, model: &Model, frame: Frame) {
    let window = app.main_window();
    let device = window.swap_chain_device();

    {
        let mut encoder = frame.command_encoder();

        let uniforms = FragmentUniforms::new(
            app.time,
            window.inner_size_pixels().0 as f32,
            window.inner_size_pixels().1 as f32,
            1. - abs(app.mouse.x - window.rect().w() / 2.) / window.rect().w(),
            1. - abs(app.mouse.y - window.rect().h() / 2.) / window.rect().h(),
        )
        .std140();

        let uniforms_bytes = shader::uniforms_as_bytes(&uniforms);
        let usage = wgpu::BufferUsage::COPY_SRC;
        let new_uniforms_buffer = device.create_buffer_with_data(uniforms_bytes, usage);

        let uniforms_size = std::mem::size_of::<UFragmentUniforms>() as wgpu::BufferAddress;

        encoder.copy_buffer_to_buffer(
            &new_uniforms_buffer,
            0,
            &model.fs_pipeline.frag_uniforms_buffer,
            0,
            uniforms_size,
        );

        let mut render_pass = wgpu::RenderPassBuilder::new()
            .color_attachment(frame.texture_view(), |color| color)
            .begin(&mut encoder);
        render_pass.set_bind_group(0, &model.fs_pipeline.bind_group, &[]);
        render_pass.set_pipeline(&model.fs_pipeline.render_pipeline);
        render_pass.set_vertex_buffer(0, &model.fs_pipeline.vertex_buffer, 0, 0);
        let vertex_range = 0..VERTICES.len() as u32;
        let instance_range = 0..1;
        render_pass.draw(vertex_range, instance_range);
    }
}
