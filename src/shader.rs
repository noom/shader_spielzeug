use nannou::{prelude::*, wgpu};

use shaderc;

use glsl_layout::{self as glsl, AsStd140};

use noise::{NoiseFn, OpenSimplex, Perlin, SuperSimplex};

use std::{fs::File, io::Read};

use crate::consts::*;
use crate::sim::*;

#[derive(Clone, Copy)]
pub struct Vertex {
    pub position: [f32; 2],
}

#[derive(Copy, Clone, AsStd140)]
pub struct FragmentUniforms {
    time: glsl::float,
    resolution: glsl::vec2,
    mouse: glsl::vec2,
}

pub type UFragmentUniforms = <FragmentUniforms as glsl::AsStd140>::Std140;

impl FragmentUniforms {
    pub fn new(time: f32, rx: f32, ry: f32, mx: f32, my: f32) -> Self {
        FragmentUniforms {
            time,
            resolution: [rx, ry].into(),
            mouse: [mx, my].into(),
        }
    }
}

type ShaderArtifacts = (shaderc::CompilationArtifact, shaderc::CompilationArtifact);

// TODO: put the const in consts.rs
const NOISE_TEXTURE_WIDTH: usize = 256;
type NoiseData = [[f32; 4]; NOISE_TEXTURE_WIDTH * NOISE_TEXTURE_WIDTH];

pub fn uniforms_as_bytes(uniforms: &UFragmentUniforms) -> &[u8] {
    unsafe { wgpu::bytes::from(uniforms) }
}

fn vertices_as_bytes(data: &[Vertex]) -> &[u8] {
    unsafe { wgpu::bytes::from_slice(data) }
}

fn create_noise_data() -> NoiseData {
    let mut data: NoiseData = [[0.; 4]; NOISE_TEXTURE_WIDTH * NOISE_TEXTURE_WIDTH];

    // Approximate output ranges
    // SuperSimplex: -0.99, 0.99
    // OpenSimplex: -0.5, 0.5
    // Perlin: -1, 1
    let simplex = SuperSimplex::new();

    for y in 0..NOISE_TEXTURE_WIDTH {
        for x in 0..NOISE_TEXTURE_WIDTH {
            // let value = simplex.get([x as f64 * 0.1, y as f64 * 0.1]) as f32;
            // let value = random_range(0, 2) as f32;
            let r: f32 = random_range(0., 1.);
            let g: f32 = random_range(0., 1.);
            let b: f32 = random_range(0., 1.);

            data[y * NOISE_TEXTURE_WIDTH + x] = [r, g, b, 1.];
        }
    }

    data
}

fn noise_data_to_bytes(data: &NoiseData) -> &[u8] {
    let len = std::mem::size_of::<NoiseData>();
    let ptr = data.as_ptr() as *const u8;

    unsafe { std::slice::from_raw_parts(ptr, len) }
}

pub fn compile_default_shader() -> ShaderArtifacts {
    // Compile shaders
    let (vert_artifact, frag_artifact) = {
        let mut compiler = shaderc::Compiler::new().unwrap();
        (
            compiler
                .compile_into_spirv(
                    &DEFAULT_VS,
                    shaderc::ShaderKind::Vertex,
                    "shader.glsl",
                    "main",
                    None,
                )
                .unwrap(),
            compiler
                .compile_into_spirv(
                    &DEFAULT_FS,
                    shaderc::ShaderKind::Fragment,
                    "shader.glsl",
                    "main",
                    None,
                )
                .unwrap(),
        )
    };

    (vert_artifact, frag_artifact)
}

pub fn compile_custom_shader() -> shaderc::Result<ShaderArtifacts> {
    let (vs_src, fs_src) = {
        let mut vs_src = String::new();
        let mut fs_src = String::new();

        File::open("./shaders/shader.vert")
            .unwrap()
            .read_to_string(&mut vs_src)
            .unwrap();
        File::open("./shaders/shader.frag")
            .unwrap()
            .read_to_string(&mut fs_src)
            .unwrap();

        (vs_src, fs_src)
    };

    // Compile shaders
    let (vert_artifact, frag_artifact) = {
        let mut compiler = shaderc::Compiler::new().unwrap();
        (
            compiler.compile_into_spirv(
                &vs_src,
                shaderc::ShaderKind::Vertex,
                "shader.glsl",
                "main",
                None,
            )?,
            compiler.compile_into_spirv(
                &fs_src,
                shaderc::ShaderKind::Fragment,
                "shader.glsl",
                "main",
                None,
            )?,
        )
    };

    Ok((vert_artifact, frag_artifact))
}

pub fn create_frag_shader_pipeline(
    app: &App,
    shader_artifacts: ShaderArtifacts,
) -> FragShaderPipeline {
    let w_id = app.window_id();
    let window = app.window(w_id).unwrap();
    let device = window.swap_chain_device();
    let queue = window.swap_chain_queue();
    let mut encoder =
        device.create_command_encoder(&wgpu::CommandEncoderDescriptor { label: None });
    let msaa_samples = window.msaa_samples();

    let (vert_artifact, frag_artifact) = shader_artifacts;

    let vs = vert_artifact.as_binary_u8();
    let vs_spirv =
        wgpu::read_spirv(std::io::Cursor::new(&vs[..])).expect("failed to read hard-coded SPIRV");
    let vs_mod = device.create_shader_module(&vs_spirv);
    let fs = frag_artifact.as_binary_u8();
    let fs_spirv =
        wgpu::read_spirv(std::io::Cursor::new(&fs[..])).expect("failed to read hard-coded SPIRV");
    let fs_mod = device.create_shader_module(&fs_spirv);

    // Initialize the texture
    let texture = wgpu::TextureBuilder::new()
        .size([NOISE_TEXTURE_WIDTH as u32, NOISE_TEXTURE_WIDTH as u32])
        // .format(wgpu::TextureFormat::R32Float)
        .format(wgpu::TextureFormat::Rgba32Float)
        .usage(wgpu::TextureUsage::COPY_DST | wgpu::TextureUsage::SAMPLED)
        .build(device);
    let texture_view = texture.view().build();

    let noise_data = create_noise_data();
    texture.upload_data(device, &mut encoder, noise_data_to_bytes(&noise_data));

    queue.submit(&[encoder.finish()]);

    // Create the sampler for sampling from the source texture.
    let sampler = wgpu::SamplerBuilder::new()
        .address_mode(wgpu::AddressMode::Repeat)
        .build(device);

    // Create the buffer that will store time
    let frag_uniforms = FragmentUniforms::new(0., 0., 0., 0., 0.).std140();
    let frag_uniforms_bytes = uniforms_as_bytes(&frag_uniforms);
    let usage = wgpu::BufferUsage::UNIFORM | wgpu::BufferUsage::COPY_DST;
    let frag_uniforms_buffer = device.create_buffer_with_data(frag_uniforms_bytes, usage);

    let bind_group_layout = wgpu::BindGroupLayoutBuilder::new()
        .uniform_buffer(wgpu::ShaderStage::FRAGMENT, false)
        .sampled_texture(
            wgpu::ShaderStage::FRAGMENT,
            false,
            wgpu::TextureViewDimension::D2,
            texture_view.component_type(),
        )
        .sampler(wgpu::ShaderStage::FRAGMENT)
        .build(device);
    let bind_group = wgpu::BindGroupBuilder::new()
        .buffer::<UFragmentUniforms>(&frag_uniforms_buffer, 0..1)
        .texture_view(&texture_view)
        .sampler(&sampler)
        .build(device, &bind_group_layout);

    let pipeline_layout = {
        let desc = wgpu::PipelineLayoutDescriptor {
            bind_group_layouts: &[&bind_group_layout],
        };
        device.create_pipeline_layout(&desc)
    };
    let render_pipeline = wgpu::RenderPipelineBuilder::from_layout(&pipeline_layout, &vs_mod)
        .fragment_shader(&fs_mod)
        .color_format(Frame::TEXTURE_FORMAT)
        .add_vertex_buffer::<Vertex>(&wgpu::vertex_attr_array![0 => Float2])
        .sample_count(msaa_samples)
        .primitive_topology(wgpu::PrimitiveTopology::TriangleStrip)
        .build(device);

    // Create the vertex buffer.
    let vertices_bytes = vertices_as_bytes(&VERTICES[..]);
    let usage = wgpu::BufferUsage::VERTEX;
    let vertex_buffer = device.create_buffer_with_data(vertices_bytes, usage);

    let fs_pipeline = FragShaderPipeline {
        bind_group,
        render_pipeline,
        vertex_buffer,
        frag_uniforms_buffer,
        texture,
    };

    fs_pipeline
}
