use nannou::prelude::*;

use notify::RecommendedWatcher;

use std::sync::mpsc;

pub struct FragShaderPipeline {
    pub bind_group: wgpu::BindGroup,
    pub render_pipeline: wgpu::RenderPipeline,
    pub vertex_buffer: wgpu::Buffer,
    pub frag_uniforms_buffer: wgpu::Buffer,
    pub texture: wgpu::Texture,
}

pub struct Model {
    pub watcher: RecommendedWatcher,
    pub shader_io_receiver: mpsc::Receiver<notify::Event>,

    pub fs_pipeline: FragShaderPipeline,

    pub last_time: f32,

    pub iteration: u32,
}

impl Model {
    pub fn new(
        watcher: RecommendedWatcher,
        shader_io_receiver: mpsc::Receiver<notify::Event>,
        fs_pipeline: FragShaderPipeline,
        last_time: f32,
    ) -> Self {
        Model {
            watcher,
            shader_io_receiver,
            fs_pipeline,
            iteration: 0,
            last_time,
        }
    }
}
